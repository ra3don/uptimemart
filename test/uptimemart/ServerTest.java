/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uptimemart;

import model.Server;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nick
 */
public class ServerTest {
    
    public ServerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of IsSaved method, of class Server.
     */
    @Test
    public void testIsSaved() {
        System.out.println("IsSaved");
        boolean expResult = false;
        boolean result = Server.IsSaved();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIsSaved method, of class Server.
     */
    @Test
    public void testSetIsSaved() {
        System.out.println("setIsSaved");
        boolean isSaved = false;
        Server.setIsSaved(isSaved);
    }

    /**
     * Test of getIP_Address method, of class Server.
     */
    @Test
    public void testGetIP_Address() {
        System.out.println("getIP_Address");
        Server instance = new Server();
        String expResult = "";
        String result = instance.getURL();
        assertEquals(expResult, result);
    }

    /**
     * Test of setIP_Address method, of class Server.
     */
    @Test
    public void testSetIP_Address() {
        System.out.println("setIP_Address");
        String ip_address = "";
        Server instance = new Server();
        instance.setURL(ip_address);
    }

    /**
     * Test of setID method, of class Server.
     */
    @Test
    public void testSetID() {
        System.out.println("setID");
        int id = 0;
        Server instance = new Server();
        instance.setID(id);
    }

    /**
     * Test of getID method, of class Server.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        int expResult = 0;
        int result = Server.getID();
        assertEquals(expResult, result);
    }

    /**
     * Test of getByIP method, of class Server.
     */
    @Test
    public void testGetByIP() {
        System.out.println("getByIP");
        String Ip = "";
        Server instance = new Server();
        Server expResult = null;
        Server result = instance.getByIP(Ip);
        assertEquals(expResult, result);
    }

    /**
     * Test of get method, of class Server.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        int id = 0;
        Server instance = new Server();
        Server expResult = null;
        Server result = instance.get(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAll method, of class Server.
     */
    @Test
    public void testGetAll() {
        System.out.println("getAll");
        ArrayList expResult = null;
        ArrayList result = Server.getAll();
        assertEquals(expResult, result);
    }

    /**
     * Test of save method, of class Server.
     */
    @Test
    public void testSave() {
        System.out.println("save");
        Server instance = new Server();
        boolean expResult = false;
        boolean result = instance.save();
        assertEquals(expResult, result);
    }

    /**
     * Test of remove method, of class Server.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        int id = 0;
        Server instance = new Server();
        boolean expResult = false;
        boolean result = instance.remove(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeAll method, of class Server.
     */
    @Test
    public void testRemoveAll() {
        System.out.println("removeAll");
        boolean expResult = false;
        boolean result = Server.removeAll();
        assertEquals(expResult, result);

    }
}
