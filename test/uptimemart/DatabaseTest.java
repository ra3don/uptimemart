/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uptimemart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nick
 */
public class DatabaseTest {
    
    public DatabaseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getConnection method, of class Database.
     */
    @Test
    public void testGetConnection() throws SQLException {
        System.out.println("getConnection");
        Connection expResult =Database.getConnection();
        Connection result = Database.getConnection();
        assertEquals(expResult, result);
    }

    /**
     * Test of initialize method, of class Database.
     */
    @Test
    public void testInitialize() {
        System.out.println("initialize");
        Database instance = new Database();
        instance.initialize();
    }
}
