package uptimemart;

import com.sun.net.httpserver.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Joel Clay
 * This HTTPHandler implements a rudimentary HTTP server which will return
 * a 500 error every other request.
 */
public class HTTPHandler implements HttpHandler {

    int response_count = 1;
    /**
     *
     * @param he
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange he) throws IOException {
        String response = "The server is up!";
        int http_response_code = 200; // Default to 200 OK HTTP Code
        //If odd numbered response
        if (response_count%2 == 0) {
            if (response_count%10 == 0) {
                //Sleep for 100 ms
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(HTTPHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
                response = "The server is slow!";
            } else {
                http_response_code = 500; //Set response to 500 internal error HTTP Code
                response = "";
            }
       } 
        
        he.sendResponseHeaders(http_response_code, response.length());
        OutputStream os = he.getResponseBody();
        os.write(response.getBytes());
        os.close();
        response_count++;
    }

}


