/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uptimemart;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import model.Server;

/**
 *
 * @author Joel Clay
 */
public class MonitorThread implements Runnable {

    private void startMonitor() {
        //Get the servers from the list
        DefaultListModel listModel = (DefaultListModel) view.UptimeMart.getModel();
        
        for (int i = 0; i < 1000; i++) {
            for (int l = 0; l < listModel.getSize(); l++) {
                //Current server
                Server server = (Server) listModel.getElementAt(l);
                //Debug
                System.out.println(server.getURL());

                try {
                    ExecutorService executor = Executors.newCachedThreadPool();
                    List<Future<Boolean>> statuses = null;
                    try {
                        statuses = executor.invokeAll(Arrays.asList(new Monitor(server.getURL())));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(view.UptimeMart.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //Set the status of the server
                    try {
                        server.setCurrentStatus(statuses.get(0).get());
                        listModel.set(l, server);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(view.UptimeMart.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ExecutionException ex) {
                        Logger.getLogger(view.UptimeMart.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    executor.shutdown();
                    //Sleep for 1 second
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MonitorThread.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void run() {
        startMonitor();
    }
}
