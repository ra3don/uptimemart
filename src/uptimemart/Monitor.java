/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uptimemart;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.Callable;
import model.Server;

/**
 *
 * @author Joel Clay
 */
public class Monitor implements Callable<Boolean> {

    private String url = "";
    

    /**
     *
     * @param url
     */
    public Monitor(String url) {
        setUrl(url);
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    
    /**
     *
     * @param server
     * @return
     */
    public static boolean testServer(Server server) {
        boolean successful = false;
        try {
            URL myURL = new URL(server.getURL());
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();

            //Set timeout to 500 ms
            myURLConnection.setReadTimeout(5000);

            //Connect to url
            myURLConnection.connect();

            //Fetch response code
            int rc = myURLConnection.getResponseCode();
            //HTTP Status OK
            if (rc == 200) {
                successful = true;
            }
            if (rc == 500) {
                successful = false;
            }
            System.out.println("HTTP Response: " + rc);
        } catch (MalformedURLException e) {
            // new URL() failed
        } catch (SocketTimeoutException st) {
            System.out.println("Response was too long");
            successful = false;
        } catch (IOException e) {
            // openConnection() failed
            System.out.println("open connection failed");
        }
        return successful;
    }

    private Boolean testURL(String url) {
        boolean successful = false;
        try {
            URL myURL = new URL(url);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();

            //Set timeout to 500 ms
            myURLConnection.setReadTimeout(5000);

            //Connect to url
            myURLConnection.connect();

            //Fetch response code
            int rc = myURLConnection.getResponseCode();
            //HTTP Status OK
            if (rc == 200) {
                successful = true;
            }
            if (rc == 500) {
                successful = false;
            }
            System.out.println("HTTP Response: " + rc);
        } catch (MalformedURLException e) {
            // new URL() failed
        } catch (SocketTimeoutException st) {
            System.out.println("Response was too long");
            successful = false;
        } catch (IOException e) {
            // openConnection() failed
            System.out.println("open connection failed");
        }
        return successful;
    }

    @Override
    public Boolean call() throws Exception {
        boolean successful = testURL(this.getUrl());
        return successful;
    }
}
