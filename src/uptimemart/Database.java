package uptimemart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nick, Joel, Ben Click
 */
public class Database {

    private static org.apache.derby.jdbc.EmbeddedDriver driverInstance = null;
    
     /**
     * Returns the connection to the database. 
     * <p>
     * This method always returns immediately, whether or not the database is filled.
     * @return   the connection to the database
     */
    
    public static Connection getConnection() {
        String dbURL = "jdbc:derby:db;create=true";
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbURL);
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

     /**
     * Creates the tables if the table does not already exist
     * <p>
     * This method determines whether the SERVERS database has been created, if not create it.
     *
     */
    
     public void initialize() {
        //Create the tables if the table does not already exist
        if (!serversTableExists()) {
            createServersTable();
        }
    }

     /**
     * Returns true if the tables already exist in the Database. 
     * <p>
     * This method always returns true if the tables exist.
     * @return  true if table exists false if the table needs to be created.
     */
      private boolean serversTableExists() {
        try {
            getConnection().prepareCall("SELECT count(*) FROM SERVERS").executeQuery();
            return true;
        } catch (Exception e) {
            // Catches table does not exist and other problems
            System.out.println("Servers table does not exist");
            return false;
        }
    }
    
    /**
     * Creates the SERVERS table in the Database.
     * <p>
     * This method always returns true if the tables once the table is created.
     * @return      boolean true if table is successfully created. false if the table can't be created.
     */
    
    private boolean createServersTable() {
        System.out.println("Creating servers table");
        
        String sql = "CREATE TABLE SERVERS("
                + " ID INT not null primary key"
                + " GENERATED ALWAYS AS IDENTITY "
                + "(START WITH 1, INCREMENT BY 1),"
                + " IP_ADDRESS VARCHAR(255) not NULL) ";
        //+ "PRIMARY KEY ( ID ))";
        try {
            getConnection().prepareStatement(sql).execute();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Created servers table in given database...");
        return true;
    }
}