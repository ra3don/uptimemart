/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import model.Server;

/**
 *
 * @author Joel Clay
 */
class ServerListItem extends JPanel implements ListCellRenderer {

    public ServerListItem() {
        setOpaque(true);
    }
    private String upPath = "/green_dot.png";
    private String downPath = "/red_dot.png";
    private boolean currentStatus = true;
    @Override
    public Component getListCellRendererComponent(JList list, Object server, int index, boolean isSelected, boolean cellHasFocus) {
        Server thisServer = null;
        thisServer = (Server) server;
        GridLayout gridLayout = new GridLayout(0, 2);
        setSize(10, 200);
        setLayout(gridLayout);
        removeAll();
        
        add(new JLabel(thisServer.getURL()));
        if (thisServer.isCurrentStatus()) {
            add(new JLabel(createImageIcon(upPath, "up")));
        } else {
            add(new JLabel(createImageIcon(downPath, "down")));

        }

        return this;
    }
    
    public void setServerStatus(boolean status) {
        if (status) {
            currentStatus = true;
        } else {
            currentStatus = false;
        }
        this.repaint();
    }

    protected ImageIcon createImageIcon(String path,
            String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}