package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import uptimemart.Database;

/**
 *
 * @author Nick Modifications: Ben Christa
 */
public class Server {

    /**
     * url is the url of the Server
     */
    private String url = "";
    private boolean currentStatus = true;

    /**
     * Getter for the up/down status of the server
     * @return
     */
    public boolean isCurrentStatus() {
        return currentStatus;
    }

    /**
     *  Setter for the up/down status of the server
     * @param currentStatus
     */
    public void setCurrentStatus(boolean currentStatus) {
        this.currentStatus = currentStatus;
    }
    
    // Need to keep track of whether this has been saved so taht we can get the ID
    private static boolean saved = false;

    // The ID representing the Server in the DB
    private static int ID = 0;

    /**
     * Getter that indicates whether or not the server is saved
     * @return
     */
    public static boolean IsSaved() {
        return saved;
    }

    /**
     * Setter that tells whether or not the server has already been saved.
     * @param isSaved
     */
    public static void setIsSaved(boolean isSaved) {
        Server.saved = isSaved;
    }

    /**
     * getter for the server's URL
     * @return
     */
    public String getURL() {
        return url;
    }

    /**
     * This sets the server's URL
     * @param url_param
     */
    public void setURL(String url_param) {
        this.url = url_param;
    }
    
    /**
     *  A getter for the server ID
     * @return
     */
    public static int getID() {
        if (IsSaved()) {
            return ID;
        } else {
            System.out.println("Has not yet been saved");
            return 0;
        }
    }

    /**
     * contructs a server object with the public variables set to the specified
     * inputs
     * @param url_param
     */
    public Server(String url_param) {
        url = url_param;
    }

    
    /**
     * input: Ip address of the server to find the id for. returns the ID of the
     * server in the data base to be used as a foreign key in the Response Table
     * @param url_param
     * @return  
     */
    public static int get(String url_param) {
        Connection connection = Database.getConnection();
        int Id = -1;
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ID FROM SERVERS WHERE IP_ADDRESS='" + url_param + "'");
            if (rs.next()) {
                Id = rs.getInt("ID");
            } else {
                Id = -2;
            }
            //System.out.println("Value found: "+Id+"\n");
            //           rs.close();
            //stmt.close();
            //connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Id;
    }

    /**
     * input:none Returns an arraylist of Server Objects using the information
     * in the Servers table.
     * @return 
     */
    public static ArrayList<Server> getAll() {
        ArrayList<Server> list = new ArrayList();
        System.out.println("GetAll was called");
        try {
            ResultSet rs = Database.getConnection().prepareStatement("SELECT * FROM SERVERS").executeQuery();
            while (rs.next()) {
                int Id = rs.getInt("ID");
                String IpAddress = rs.getString("IP_ADDRESS");
//                System.out.println(Id);
//                System.out.println(IpAddress);
                list.add(new Server(IpAddress));
            }
        } catch (SQLException ex) {
            System.out.println("An error occured");
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    /**
     * input: ip address of the server to insert. Inserts the server into the
     * Server Table.
     * @return 
     */
    public boolean save() {
        Connection connection = Database.getConnection();
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.execute("INSERT INTO SERVERS (IP_ADDRESS) VALUES ('" + url + "')");
            //Record has been saved at this point
            setIsSaved(true);
        } catch (SQLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    /**
     * input: id of the server to delete. deletes the server from the Server
     * table.
     * @param id 
     * @return 
     */
    public boolean remove(int id) {
        Connection connection = Database.getConnection();
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("DELETE FROM SERVERS WHERE ID=" + id);
            //          rs.close();
            //stmt.close();
            //connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    /**
     * input: none. Empties the table.
     * @return 
     */
    public static boolean removeAll() {
        try {
            Database.getConnection().prepareStatement("DELETE FROM SERVERS").execute();
            //        rs.close();
            //stmt.close();
            //connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
